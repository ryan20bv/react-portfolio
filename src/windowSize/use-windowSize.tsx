import React from "react";
import { useState, useLayoutEffect, useEffect } from "react";

export function useWindowSize() {
	const [size, setSize] = useState<number[]>([0, 0]);
	useLayoutEffect(() => {
		function updateSize() {
			setSize([window.innerWidth, window.innerHeight]);
		}
		window.addEventListener("resize", updateSize);
		updateSize();
		return () => window.removeEventListener("resize", updateSize);
	}, []);
	return size;
}

export const useScrollHandler = () => {
	const [scrollPosition, setScrollPosition] = useState<number>(0);
	const handleScroll = () => {
		const position = window.pageYOffset;
		setScrollPosition(position);
	};

	useEffect(() => {
		window.addEventListener("scroll", handleScroll, { passive: true });

		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, []);

	return scrollPosition;
};

export const useClientHeight = (element: React.RefObject<HTMLDivElement>) => {
	const [elementHeight, setElementHeight] = useState<number>(0);

	useEffect(() => {
		const height = element.current!.clientHeight;
		setElementHeight(height);
	}, [element]);
	return elementHeight;
};
