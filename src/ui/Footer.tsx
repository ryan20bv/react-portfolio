import classes from "../scss/footer.module.scss";
const Footer = () => {
	return (
		<div className={classes.footer}>
			<p>ryan20bv@gmail.com</p>
		</div>
	);
};
export default Footer;
