import React from "react";
import classes from "../scss/burgerMenu.module.scss";
interface PropsType {
	burgerMenuIsOpen: boolean;
	burgerMenuHandler: () => void;
	where?: string;
}

const BurgerMenu: React.FC<PropsType> = (props) => {
	const burgerClass = props.burgerMenuIsOpen && `${classes.change}`;
	let addedClass: boolean | string;
	addedClass = props.where === "About" ? "black" : "";
	return (
		<section
			className={`${classes.burger_menu} ${burgerClass}`}
			onClick={props.burgerMenuHandler}
		>
			<div
				className={classes.bar1}
				style={{ backgroundColor: addedClass }}
			></div>
			<div
				className={classes.bar2}
				style={{ backgroundColor: addedClass }}
			></div>
			<div
				className={classes.bar3}
				style={{ backgroundColor: addedClass }}
			></div>
		</section>
	);
};

export default BurgerMenu;
