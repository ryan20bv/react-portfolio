import React, { useRef } from "react";
import {
	useWindowSize,
	useScrollHandler,
	useClientHeight,
} from "../windowSize/use-windowSize";
import ScrollNav from "./ScrollNav";
import classes from "../scss/divWrapper.module.scss";

interface PropsType {
	leftContent: React.ReactNode;
	rightContent: React.ReactNode;
	bgColor: string;
	where: string;
}

const DivWrapper: React.FC<PropsType> = (props) => {
	const leftRef = useRef<HTMLDivElement>(null);

	const [windowWidth] = useWindowSize();
	const isLessThan: boolean = windowWidth < 1000;
	const scrollPosition: number = useScrollHandler();
	const elementHeight: number = useClientHeight(leftRef);
	const scrollMoreThenLeftRef: boolean = scrollPosition > elementHeight - 70;

	return (
		<section className={classes.wrapper}>
			<div className={classes.left} ref={leftRef}>
				{props.leftContent}
			</div>
			{isLessThan && scrollMoreThenLeftRef && (
				<ScrollNav bgColor={props.bgColor} where={props.where} />
			)}

			<div className={classes.right}>{props.rightContent}</div>
		</section>
	);
};

export default DivWrapper;
