import React, { useState } from "react";

import Nav from "./Nav";
import BurgerMenu from "./BurgerMenu";

import classes from "../scss/scrollNav.module.scss";
interface PropsType {
	bgColor: string;
	where: string;
}

const ScrollNav: React.FC<PropsType> = (props) => {
	const [burgerMenuIsOpen, setBurgerMenuIsOpen] = useState<boolean>(false);

	const burgerMenuHandler = () => {
		setBurgerMenuIsOpen((prevData) => !prevData);
	};

	const optionClass = !burgerMenuIsOpen && `${classes.display}`;

	return (
		<div
			className={classes.scroll_main}
			style={{ backgroundColor: props.bgColor }}
		>
			<div className={classes.insideMain}>
				<header>
					<img src={require("./assets/images/dog.jpeg")} alt='logo' />
					<h2>RBV</h2>
				</header>
				<h1>{props.where}</h1>
				<div className={classes.magic_nav}>
					<div
						className={`${classes.view} ${optionClass}`}
						style={{ backgroundColor: props.bgColor }}
					>
						<Nav
							head='About'
							fontFamily='Satisfy'
							headColor='white'
							listColor='black'
							addedClass={classes.scroll_nav}
						/>
					</div>
					<BurgerMenu
						burgerMenuIsOpen={burgerMenuIsOpen}
						burgerMenuHandler={burgerMenuHandler}
						where={props.where}
					/>
				</div>
			</div>
		</div>
	);
};

export default ScrollNav;
