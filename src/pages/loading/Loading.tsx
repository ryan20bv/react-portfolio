import classes from "../../scss/loading.module.scss";
const Loading = () => {
	return (
		<div className={classes.loading}>
			<h1>RBV</h1>
		</div>
	);
};

export default Loading;
