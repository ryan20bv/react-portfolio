import React, { useState, useRef, useEffect } from "react";
import { HiArrowCircleLeft, HiArrowCircleRight } from "react-icons/hi";

import classes from "../../scss/miniApp/projectCarousel.module.scss";

interface PropsType {
	project: {
		title: string;
		description: string;
		img: string[];
		id: string;
		link: string;
	};
}
const ProjectCarousel: React.FC<PropsType> = ({ project }) => {
	const imageDivRef = useRef<HTMLDivElement>(null);
	const [leftShownItem, setLeftShownItem] = useState<number>(0);
	const [midShownItem, setMidShownItem] = useState<number>(1);
	const [rightShownItem, setRightShownItem] = useState<number>(2);
	const [isSelected, setIsSelected] = useState(true);

	const origLength: number = project.img.length;

	useEffect(() => {
		if (!isSelected) {
			timeOutHandler();
		}
	}, [isSelected]);

	const leftChangeHandler = () => {
		// imageDivRef.current?.classList.add(`${classes.opaque}`);
		let newLeft = leftShownItem - 1;
		let newMid = midShownItem - 1;
		let newRight = rightShownItem - 1;
		if (newLeft < 0) {
			newLeft = origLength - 1;
		}
		if (newMid < 0) {
			newMid = origLength - 1;
		}
		if (newRight < 0) {
			newRight = origLength - 1;
		}

		setLeftShownItem(newLeft);
		setMidShownItem(newMid);
		setRightShownItem(newRight);

		setIsSelected(false);
	};
	const rightChangeHandler = () => {
		// imageDivRef.current?.classList.add(`${classes.opaque}`);
		let newLeft = leftShownItem + 1;
		let newMid = midShownItem + 1;
		let newRight = rightShownItem + 1;
		if (newLeft > origLength - 1) {
			newLeft = 0;
		}
		if (newMid > origLength - 1) {
			newMid = 0;
		}
		if (newRight > origLength - 1) {
			newRight = 0;
		}

		setLeftShownItem(newLeft);
		setMidShownItem(newMid);
		setRightShownItem(newRight);

		setIsSelected(false);
	};

	const timeOutHandler = () => {
		let timerId: number = 0;
		if (!isSelected) {
			timerId = window.setTimeout(() => {
				setIsSelected(true);
			}, 100);
		}
	};

	return (
		<div className={classes.projectCarousel}>
			<div className={classes.top}>
				<HiArrowCircleLeft
					className={classes.arrow}
					onClick={leftChangeHandler}
				/>
				<div className={classes.container}>
					{project.img.map((eachImg, index) => (
						<div
							key={index}
							className={`${
								index === leftShownItem ? classes.opaque : classes.hidden
							} ${classes.leftImage}`}
							ref={imageDivRef}
						>
							<img
								src={require(`./assets/images/${eachImg}`)}
								alt={project.title}
							/>
						</div>
					))}
					{project.img.map((eachImg, index) => (
						<div
							key={index}
							className={`${
								index === midShownItem
									? isSelected
										? classes.selected
										: classes.opaque
									: classes.hidden
							} ${classes.middleImage}`}
							ref={imageDivRef}
						>
							<img
								src={require(`./assets/images/${eachImg}`)}
								alt={project.title}
							/>
						</div>
					))}
					{project.img.map((eachImg, index) => (
						<div
							key={index}
							className={`${
								index === rightShownItem ? classes.opaque : classes.hidden
							} ${classes.rightImage}`}
							ref={imageDivRef}
						>
							<img
								src={require(`./assets/images/${eachImg}`)}
								alt={project.title}
							/>
						</div>
					))}
				</div>
				<HiArrowCircleRight
					className={classes.arrow}
					onClick={rightChangeHandler}
				/>
			</div>

			<div className={classes.details}>
				<div>
					<input type='radio' checked readOnly />
					<a href={project.link} target='_blank' rel='noreferrer'>
						<h2>{project.title} </h2>
					</a>
				</div>

				<p>{project.description}</p>
			</div>
		</div>
	);
};

export default ProjectCarousel;
