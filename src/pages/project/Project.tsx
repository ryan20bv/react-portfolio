import DivWrapper from "../../ui/DivWrapper";
import Nav from "../../ui/Nav";
import ProjectRight from "./ProjectRight";
import Footer from "../../ui/Footer";
import classes from "../../scss/project.module.scss";

const Project = () => {
	const leftComponent = (
		<main className={classes.project_left}>
			<Nav
				head='Project'
				fontFamily='Satisfy'
				headColor='red'
				listColor='red'
				addedClass={classes.project_nav}
			/>
		</main>
	);
	const rightComponent = <ProjectRight />;
	return (
		<>
			<DivWrapper
				leftContent={leftComponent}
				rightContent={rightComponent}
				bgColor='gray'
				where='Project'
			/>
			<Footer />
		</>
	);
};

export default Project;
