import ProjectCarousel from "./ProjectCarousel";
import classes from "../../scss/projectRight.module.scss";

const Items = [
	{
		id: "1",
		title: "Pokemon",
		description: "Fetch list of pokemon from pokemon.io api using redux",
		img: ["newPoke4.PNG", "newPoke1.PNG", "newPoke2.PNG"],
		link: "https://gotta-catch-them.netlify.app/",
	},
	{
		id: "2",
		title: "Pizzalated",
		description: "An pizza e-commerce using redux and firebase database",
		img: ["newPizza3.PNG", "newPizza1.PNG", "newPizza2.PNG"],
		link: "https://pizzalated-efb81.web.app/ ",
	},
	{
		id: "3",
		title: "Mini-TypeScript",
		description:
			"A mini project to exercise TypeScript and Flowbite a tailwind library",
		img: ["newMini3.PNG", "newMini1.PNG", "newMini2.PNG"],
		link: "https://mini-typescript.vercel.app/ ",
	},
];

const ProjectRight = () => {
	return (
		<main className={classes.right}>
			{Items.map((item, index) => (
				<ProjectCarousel project={item} key={index} />
			))}
		</main>
	);
};

export default ProjectRight;
