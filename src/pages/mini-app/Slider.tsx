import React, { useRef, useEffect, useState } from "react";
import { HiArrowCircleLeft, HiArrowCircleRight } from "react-icons/hi";
import classes from "../../scss/slider.module.scss";

interface PropsType {
	children: React.ReactNode;
	outPutClientWidth: number;
	burgerMenuIsOpen: boolean;
}

const Slider: React.FC<PropsType> = (props) => {
	const sliderDivRef = useRef<HTMLDivElement>(null);
	const [screenXValue, setScreenXValue] = useState<number>(0);

	onmousemove = (e) => {
		setScreenXValue(e.clientX);
	};

	useEffect(() => {
		sliderDivRef.current!.scrollLeft = 0;
	}, []);

	const value: number = props.outPutClientWidth / 6;

	const moveSlider = (): void => {
		if (screenXValue < value) {
			sliderDivRef.current!.scrollLeft = 0;
		} else if (screenXValue >= value && screenXValue < value * 2) {
			sliderDivRef.current!.scrollLeft = 80;
		} else if (screenXValue >= value * 2 && screenXValue < value * 3) {
			sliderDivRef.current!.scrollLeft = 160;
		} else if (screenXValue >= value * 3 && screenXValue < value * 4) {
			sliderDivRef.current!.scrollLeft = 218;
		} else if (screenXValue >= value * 4) {
			sliderDivRef.current!.scrollLeft = 350;
		}
	};
	const leftScrollHandler = (): void => {
		if (sliderDivRef.current!.scrollLeft <= 0) {
			return;
		}

		sliderDivRef.current!.scrollLeft -= 150;
	};
	const rightScrollHandler = (): void => {
		if (sliderDivRef.current!.scrollLeft >= 300) {
			return;
		}

		sliderDivRef.current!.scrollLeft += 150;
	};

	const enterSliderDivHandler = (): void => {
		moveSlider();
	};
	const addedClass = props.burgerMenuIsOpen && `${classes.open}`;
	return (
		<div className={`${classes.slider_outer} ${addedClass}`}>
			<HiArrowCircleLeft
				className={classes.slider_arrow}
				onClick={leftScrollHandler}
			/>

			<div
				className={`${classes.slider_div} ${addedClass}`}
				ref={sliderDivRef}
				onMouseEnter={enterSliderDivHandler}
				onMouseMove={moveSlider}
			>
				{props.children}
			</div>

			<HiArrowCircleRight
				onClick={rightScrollHandler}
				className={classes.slider_arrow}
			/>
		</div>
	);
};

export default Slider;
