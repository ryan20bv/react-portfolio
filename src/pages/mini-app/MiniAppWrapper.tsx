import React, { useRef, useState, useEffect } from "react";
import { useWindowSize } from "../../windowSize/use-windowSize";
import Nav from "../../ui/Nav";
import Slider from "./Slider";
import SliderContainer from "./SliderContainer";
import BurgerMenu from "../../ui/BurgerMenu";
import Footer from "../../ui/Footer";

import classes from "../../scss/minAppWrapper.module.scss";

interface PropsType {
	children: React.ReactNode;
}

const MiniAppWrapper: React.FC<PropsType> = (props) => {
	const [windowWidth] = useWindowSize();
	const smallWindow = windowWidth < 1000;
	const outPutRef = useRef<HTMLDivElement>(null);
	const [burgerMenuIsOpen, setBurgerMenuIsOpen] = useState(false);

	const [outPutClientWidth, setOutPutClientWidth] = useState<number>(0);

	useEffect(() => {
		const updatedClientWidth = outPutRef.current!.clientWidth;
		setOutPutClientWidth(updatedClientWidth);
	}, []);
	const burgerMenuHandler = () => {
		setBurgerMenuIsOpen((prevData) => !prevData);
	};
	const closeBurgerMenuHandler = () => {
		setBurgerMenuIsOpen(false);
	};

	return (
		<main className={classes.main}>
			<section className={classes.nav_container}>
				<Nav addedClass={classes.mini_app_nav} />
				{smallWindow && (
					<div className={classes.miniApp_burger}>
						<BurgerMenu
							burgerMenuHandler={burgerMenuHandler}
							burgerMenuIsOpen={burgerMenuIsOpen}
						/>
					</div>
				)}
			</section>

			<div
				className={classes.output}
				ref={outPutRef}
				onClick={closeBurgerMenuHandler}
			>
				{props.children}
			</div>

			<Slider
				outPutClientWidth={outPutClientWidth}
				burgerMenuIsOpen={burgerMenuIsOpen}
			>
				<SliderContainer burgerMenuIsOpen={burgerMenuIsOpen} />
			</Slider>
			<Footer />
		</main>
	);
};

export default MiniAppWrapper;
