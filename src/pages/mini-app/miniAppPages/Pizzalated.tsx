import { useRef, useState } from "react";

import PizzaNav from "./pizzaComponents/pizzaNav";
import { useWindowSize } from "../../../windowSize/use-windowSize";
import PizzaContent from "./pizzaComponents/PizzaContent";

import { HiArrowCircleLeft, HiArrowCircleRight } from "react-icons/hi";

import classes from "../../../scss/miniApp/pizzalated.module.scss";
const Pizzalated = () => {
	const pizzaWrapperRef = useRef<HTMLDivElement>(null);
	const [windowWidth] = useWindowSize();
	const [searchValue, setSearchValue] = useState<string>("");

	const leftArrowHandler = () => {
		if (pizzaWrapperRef.current!.scrollLeft === 0) {
			return;
		}
		pizzaWrapperRef.current!.scrollLeft -= 390;
	};
	const rightArrowHandler = () => {
		if (pizzaWrapperRef.current!.scrollLeft >= 1200) {
			return;
		}
		pizzaWrapperRef.current!.scrollLeft += 390;
	};
	const isWindowAbove600 = windowWidth >= 600;
	const updateSearchValue = (value: string) => {
		setSearchValue(value);
	};

	return (
		<main className={classes.pizzalated} id='backdrop-root'>
			<PizzaNav updateSearchValue={updateSearchValue} />

			<section>
				{isWindowAbove600 && (
					<HiArrowCircleLeft
						onClick={leftArrowHandler}
						className={classes.arrow_left}
					/>
				)}

				<div className={classes.pizza_wrapper} ref={pizzaWrapperRef}>
					<PizzaContent windowWidth={windowWidth} searchValue={searchValue} />
				</div>
				{isWindowAbove600 && (
					<HiArrowCircleRight
						onClick={rightArrowHandler}
						className={classes.arrow_right}
					/>
				)}
			</section>
			<footer>
				<p>
					Pictures from{" "}
					<b>
						<a
							href='https://www.ezcater.com/lunchrush/office/most-popular-types-of-pizza-around-country/'
							target='_blank'
							rel='noreferrer'
						>
							EZCATER
						</a>
					</b>
				</p>
				<p>
					Full-page{" "}
					<b>
						<a
							href='https://pizzalated-efb81.web.app/'
							target='_blank'
							rel='noreferrer'
						>
							Pizzalated
						</a>
					</b>{" "}
				</p>
			</footer>
		</main>
	);
};

export default Pizzalated;
