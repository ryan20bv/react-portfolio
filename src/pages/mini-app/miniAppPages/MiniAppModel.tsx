export class TodoClass {
	constructor(public id: string, public task: string, public isDone: boolean) {}
}

export class PizzaClass {
	constructor(
		public id: string,
		public img: string,
		public flavor: string,
		public price: number
	) {}
}
