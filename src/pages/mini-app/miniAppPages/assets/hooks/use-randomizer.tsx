import { useEffect, useState } from "react";

const useRandomizer = () => {
	const [maxValue, setMaxValue] = useState<number>(42);
	const [arrayValues, setArrayValues] = useState<string[]>([
		"??",
		"??",
		"??",
		"??",
		"??",
		"??",
	]);
	const [arrayIndex, setArrayIndex] = useState<number>(0);
	const [isAutomatic, setIsAutomatic] = useState<boolean>(false);
	let isFound: boolean;
	let intervalId: number;
	const saveToArray = (value: string, currentIndex: number) => {
		setArrayValues((prevData) => {
			return prevData.map((item, index) => {
				if (index === currentIndex) {
					return value;
				}
				return item;
			});
		});
	};

	const randomizedNumber = (currentIndex: number) => {
		let newRandomNumber: string;

		const randomFunction = () => {
			if (intervalId) {
				clearInterval(intervalId);
			}
			intervalId = window.setInterval(() => {
				newRandomNumber = Math.ceil(Math.random() * maxValue).toString();
				isFound = arrayValues.includes(newRandomNumber);
				saveToArray(newRandomNumber, currentIndex);
			}, 100);
		};
		randomFunction();

		setTimeout(() => {
			if (isFound) {
				clearInterval(intervalId);
				randomizedNumber(arrayIndex);
			} else {
				setArrayIndex((prevData) => prevData + 1);
				clearInterval(intervalId);
				if (isAutomatic) {
					if (arrayIndex >= 6) {
						setIsAutomatic(false);
						clearInterval(intervalId);
					}
				}
			}
		}, 2000);
	};

	const singleClickHandler = () => {
		if (arrayIndex < 6) {
			randomizedNumber(arrayIndex);
		}
	};

	useEffect(() => {
		if (isAutomatic && arrayIndex < 6) {
			singleClickHandler();
		} else {
			clearInterval(intervalId);
		}
	}, [isAutomatic, arrayIndex]);

	const automaticClickHandler = () => {
		setIsAutomatic(true);
	};

	const resetStateHandler = () => {
		clearInterval(intervalId);
		setArrayValues(["??", "??", "??", "??", "??", "??"]);
		setArrayIndex(0);
		setIsAutomatic(false);
		isFound = false;
	};

	const changeMaxValue = (value: number) => {
		setMaxValue(value);
	};

	return {
		maxValue,
		arrayValues,
		changeMaxValue,
		singleClickHandler,
		resetStateHandler,
		automaticClickHandler,
	};
};

export default useRandomizer;
