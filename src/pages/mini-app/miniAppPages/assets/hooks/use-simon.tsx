import { useCallback, useReducer, useEffect } from "react";

import classes from "../../../../../scss/miniApp/simon.module.scss";
const greenMP3 = require("../audio/green.mp3");
const redMP3 = require("../audio/red.mp3");
const yellowMP3 = require("../audio/yellow.mp3");
const blueMP3 = require("../audio/blue.mp3");
const wrongMp3 = require("../audio/wrong.mp3");

class GameClass {
	constructor(
		public isGameOver: boolean,
		public playerTurn: Boolean,
		public computerTurn: boolean
	) {}
}

interface SimonStateType {
	level: number;
	playerArray: string[];
	computerArray: string[];
	index: number;
	timerValue: number;
	gameStatus: GameClass;
	globalTimerId: number | null;
	isModalOpen: boolean;
}

const initialState: SimonStateType = {
	level: 1,
	playerArray: [],
	computerArray: ["green", "red", "yellow", "blue"],
	index: 0,
	timerValue: 5,
	gameStatus: {
		isGameOver: false,
		playerTurn: false,
		computerTurn: false,
	},
	globalTimerId: 0,
	isModalOpen: true,
};

type SimonActionType =
	| { type: "TEST_ACTION"; payload: {} }
	| { type: "GAME_STATUS"; payload: { gameStatus: GameClass } }
	| { type: "UPDATE_COMPUTER_ARRAY"; payload: { item: string } }
	| { type: "UPDATE_PLAYER_ARRAY"; payload: { item: string } }
	| { type: "RESET_ARRAY" }
	| { type: "UPDATE_INDEX" }
	| { type: "RESET_INDEX" }
	| { type: "UPDATE_LEVEL" }
	| { type: "RESET_LEVEL" }
	| { type: "UPDATE_TIMER" }
	| { type: "RESET_TIMER" }
	| { type: "GET_TIMER_ID"; payload: { timerId: number } }
	| { type: "RESET_TIMER_ID"; payload: {} }
	| { type: "UPDATE_MODAL"; payload: { status: boolean } };

const simonReducer = (state = initialState, action: SimonActionType) => {
	const { type } = action;
	switch (type) {
		case "UPDATE_LEVEL":
			const newLevel = state.level + 1;
			return { ...state, level: newLevel };
		case "RESET_LEVEL":
			return { ...state, level: 1 };
		case "GAME_STATUS":
			return { ...state, gameStatus: action.payload.gameStatus };
		case "RESET_ARRAY":
			return { ...state, computerArray: [], playerArray: [] };
		case "UPDATE_COMPUTER_ARRAY":
			const compArray = [...state.computerArray, action.payload.item];
			return { ...state, computerArray: compArray };
		case "UPDATE_PLAYER_ARRAY":
			const playArray = [...state.playerArray, action.payload.item];
			return { ...state, playerArray: playArray };
		case "UPDATE_INDEX":
			const newIndex = state.index + 1;
			return { ...state, index: newIndex };
		case "RESET_INDEX":
			return { ...state, index: 0 };
		case "UPDATE_TIMER":
			const newTime = state.timerValue - 1;
			return { ...state, timerValue: newTime };
		case "RESET_TIMER":
			return { ...state, timerValue: 5 };
		case "GET_TIMER_ID":
			return { ...state, globalTimerId: action.payload.timerId };
		case "RESET_TIMER_ID":
			return { ...state, globalTimerId: null };
		case "UPDATE_MODAL":
			return { ...state, isModalOpen: action.payload.status };
		default:
			return state;
	}
};

const useSimon = (
	green: React.RefObject<HTMLDivElement>,
	red: React.RefObject<HTMLDivElement>,
	yellow: React.RefObject<HTMLDivElement>,
	blue: React.RefObject<HTMLDivElement>,
	wrong: React.RefObject<HTMLDivElement>,
	timer: React.RefObject<HTMLDivElement>
) => {
	const [simonState, dispatchSimon] = useReducer(simonReducer, initialState);
	const { level, gameStatus, computerArray, index, globalTimerId, timerValue } =
		simonState;

	useEffect(() => {
		if (timerValue < 1) {
			if (globalTimerId && typeof globalTimerId === "number") {
				clearInterval(globalTimerId);
			}
			wrongAnswerHandler();
		}
	}, [timerValue]);

	useEffect(() => {
		if (gameStatus.playerTurn) {
			activateTimer();
			// timer.current!.classList.remove(`${classes.notVisible}`);
		} else if (gameStatus.computerTurn) {
			// timer.current!.classList.add(`${classes.notVisible}`);
			if (globalTimerId && typeof globalTimerId === "number") {
				clearInterval(globalTimerId);
			}
			dispatchSimon({ type: "RESET_TIMER" });
		}
	}, [gameStatus]);

	const activateTimer = () => {
		if (globalTimerId && typeof globalTimerId === "number") {
			clearInterval(globalTimerId);
		}

		const timerId: number = window.setInterval(() => {
			dispatchSimon({ type: "UPDATE_TIMER" });
		}, 1000);
		dispatchSimon({ type: "RESET_TIMER" });
		dispatchSimon({
			type: "GET_TIMER_ID",
			payload: { timerId: timerId },
		});
	};

	const changeGameStatus = (status: string) => {
		switch (status) {
			case "GAME_OVER":
				dispatchSimon({
					type: "GAME_STATUS",
					payload: {
						gameStatus: {
							isGameOver: true,
							playerTurn: false,
							computerTurn: false,
						},
					},
				});
				break;
			case "PLAYER_TURN":
				dispatchSimon({
					type: "GAME_STATUS",
					payload: {
						gameStatus: {
							isGameOver: false,
							playerTurn: true,
							computerTurn: false,
						},
					},
				});

				break;
			case "COMPUTER_TURN":
				dispatchSimon({
					type: "GAME_STATUS",
					payload: {
						gameStatus: {
							isGameOver: false,
							playerTurn: false,
							computerTurn: true,
						},
					},
				});

				break;
			case "RESET_GAME":
				dispatchSimon({ type: "RESET_TIMER" });
				dispatchSimon({ type: "RESET_LEVEL" });
				dispatchSimon({
					type: "GAME_STATUS",
					payload: {
						gameStatus: {
							isGameOver: false,
							playerTurn: false,
							computerTurn: false,
						},
					},
				});

				break;
			default:
				break;
		}
	};

	const resetGameHandler = () => {
		setTimeout(() => {
			changeGameStatus("RESET_GAME");
			wrong.current!.classList.remove(`${classes.wrong}`);
			closeModal(true);
		}, 3000);
	};

	const wrongAnswerHandler = useCallback(() => {
		if (globalTimerId && typeof globalTimerId === "number") {
			clearInterval(globalTimerId);
		}
		changeGameStatus("GAME_OVER");
		playSound("wrong");
		resetGameHandler();
	}, []);

	const playSound = (type: string) => {
		switch (type) {
			case "green":
				green.current!.classList.add(`${classes.pressed}`);
				let greenAudio = new Audio(greenMP3);
				greenAudio.play();

				setTimeout(() => {
					green.current!.classList.remove(`${classes.pressed}`);
				}, 100);
				break;
			case "red":
				red?.current?.classList.add(`${classes.pressed}`);
				let redAudio = new Audio(redMP3);
				redAudio.play();
				setTimeout(() => {
					red.current?.classList.remove(`${classes.pressed}`);
				}, 100);
				break;
			case "yellow":
				yellow?.current?.classList.add(`${classes.pressed}`);
				let yellowAudio = new Audio(yellowMP3);
				yellowAudio.play();
				setTimeout(() => {
					yellow.current?.classList.remove(`${classes.pressed}`);
				}, 100);
				break;
			case "blue":
				blue?.current?.classList.add(`${classes.pressed}`);
				let blueAudio = new Audio(blueMP3);
				blueAudio.play();
				setTimeout(() => {
					blue.current?.classList.remove(`${classes.pressed}`);
				}, 100);
				break;
			case "wrong":
			default:
				wrong.current!.classList.add(`${classes.wrong}`);
				let wrongAudio = new Audio(wrongMp3);
				wrongAudio.play();
				break;
		}
	};

	const compareBothArray = (color: string) => {
		let copyOfLevel = level;
		if (color === computerArray[index]) {
			playSound(color);
			dispatchSimon({ type: "UPDATE_INDEX" });
			activateTimer();

			if (index === computerArray.length - 1) {
				copyOfLevel++;
				setTimeout(() => {
					dispatchSimon({ type: "UPDATE_LEVEL" });
					computerTurnHandler(copyOfLevel);
				}, 1000);
			}
		} else {
			wrongAnswerHandler();
			return;
		}
	};

	const playerClickHandler = (e: React.MouseEvent<HTMLDivElement>) => {
		if (gameStatus.playerTurn) {
			dispatchSimon({
				type: "UPDATE_PLAYER_ARRAY",
				payload: { item: e.currentTarget.id },
			});
			compareBothArray(e.currentTarget.id);
		}
	};

	const playerTurnHandler = () => {
		changeGameStatus("PLAYER_TURN");
	};

	const computerRandomHandler = (passedLevel: number) => {
		let loop = 1;

		const selectionArray = ["green", "red", "yellow", "blue"];
		const computerLoop = () => {
			if (loop <= passedLevel) {
				const randomNum = Math.floor(Math.random() * selectionArray.length);
				const selectedColor = selectionArray[randomNum];
				dispatchSimon({
					type: "UPDATE_COMPUTER_ARRAY",
					payload: { item: selectedColor },
				});
				loop++;
				playSound(selectedColor);
				setTimeout(() => {
					computerLoop();
				}, 1000);
			} else {
				playerTurnHandler();
			}
		};
		computerLoop();
	};

	const resetArrayHandler = () => {
		dispatchSimon({ type: "RESET_ARRAY" });
		dispatchSimon({ type: "RESET_INDEX" });
	};

	const computerTurnHandler = (passedLevel: number) => {
		resetArrayHandler();
		changeGameStatus("COMPUTER_TURN");

		setTimeout(() => {
			computerRandomHandler(passedLevel);
		}, 1000);
	};

	const playStartHandler = (): void => {
		closeModal(false);
		computerTurnHandler(level);
	};

	const closeModal = (status: boolean) => {
		dispatchSimon({ type: "UPDATE_MODAL", payload: { status } });
	};

	const samplePlay = useCallback(() => {
		if (globalTimerId && typeof globalTimerId === "number") {
			clearInterval(globalTimerId);
		}

		let i = 0;
		function myLoop() {
			setTimeout(function () {
				playSound(simonState.computerArray[i]);
				i++;
				if (i < simonState.computerArray.length) {
					myLoop();
				} else {
					return;
				}
			}, 500);
		}
		myLoop();
	}, []);

	return {
		samplePlay,
		playStartHandler,
		timerValue: simonState.timerValue,
		gameLevel: simonState.level,
		gameStatus: simonState.gameStatus,
		isModalOpen: simonState.isModalOpen,
		playerClickHandler,
		closeModal,
	};
};

export default useSimon;
