import { useReducer } from "react";
import { TodoClass } from "../../MiniAppModel";

const initial = [
	new TodoClass("101", "Exercise", false),
	new TodoClass("102", "Study", true),
	new TodoClass("103", "Create", false),
];

interface TodoStateStype {
	listArray: TodoClass[];
	inputValue: string;
	isNotEmpty: boolean;
	isEditing: boolean;
	itemToEdit: TodoClass;
}

const initialState: TodoStateStype = {
	listArray: initial,
	inputValue: "",
	isNotEmpty: false,
	isEditing: false,
	itemToEdit: {} as TodoClass,
};

export type ActionType =
	| { type: "CHANGE_INPUT_VALUE"; payload: { enteredValue: string } }
	| { type: "CHECK_INPUT"; payload: { status: boolean } }
	| { type: "ADD_TODO"; payload: { newTodo: TodoClass } }
	| { type: "IS_DONE_STATUS"; payload: { id: string } }
	| { type: "DELETE_TODO"; payload: { id: string } }
	| { type: "IS_EDITING_STATUS"; payload: { status: boolean; item: TodoClass } }
	| { type: "CONFIRM_EDIT"; payload: { itemToEdit: TodoClass } };

const todoReducer = (
	state: TodoStateStype,
	action: ActionType
): TodoStateStype => {
	const { type, payload } = action;
	switch (type) {
		case "CHANGE_INPUT_VALUE":
			return { ...state, inputValue: payload.enteredValue };
		case "CHECK_INPUT":
			return { ...state, isNotEmpty: payload.status };
		case "ADD_TODO":
			const newArray = [...state.listArray, payload.newTodo];
			return { ...state, listArray: newArray };
		case "IS_DONE_STATUS":
			const searchedIndex = state.listArray.findIndex((item) => {
				return item.id === payload.id;
			});
			let searchedItem = state.listArray[searchedIndex];
			searchedItem = {
				id: searchedItem.id,
				task: searchedItem.task,
				isDone: searchedItem.isDone ? false : true,
			};
			let updatedArray = [...state.listArray];
			updatedArray[searchedIndex] = searchedItem;
			return { ...state, listArray: updatedArray };
		case "DELETE_TODO":
			const reducedArray = state.listArray.filter((item) => {
				return item.id !== payload.id;
			});

			return { ...state, listArray: reducedArray };

		case "IS_EDITING_STATUS":
			return { ...state, isEditing: payload.status, itemToEdit: payload.item };
		case "CONFIRM_EDIT":
			const foundItemIndex = state.listArray.findIndex((item) => {
				return item.id === payload.itemToEdit.id;
			});

			const copyOfArray = [...state.listArray];
			copyOfArray[foundItemIndex] = {
				...payload.itemToEdit,
				task: state.inputValue,
			};
			return { ...state, listArray: copyOfArray };
		default:
			return state;
	}
};

const useTodo = (validateValue: (value: string) => boolean) => {
	const [todoState, dispatchTodo] = useReducer(todoReducer, initialState);
	const valueIsValid = validateValue(todoState.inputValue);
	const clearInputHandler = () => {
		dispatchTodo({
			type: "CHANGE_INPUT_VALUE",
			payload: { enteredValue: "" },
		});
		dispatchTodo({
			type: "CHECK_INPUT",
			payload: { status: false },
		});
	};
	const inputChangeHandler = (inputValue: string) => {
		if (inputValue.trim() !== "") {
			dispatchTodo({ type: "CHECK_INPUT", payload: { status: true } });
		} else {
			dispatchTodo({
				type: "CHECK_INPUT",
				payload: { status: false },
			});
		}

		dispatchTodo({
			type: "CHANGE_INPUT_VALUE",
			payload: { enteredValue: inputValue },
		});
	};

	const isDoneItemHandler = (id: string) => {
		dispatchTodo({
			type: "IS_DONE_STATUS",
			payload: { id },
		});
	};
	const deleteTodoHandler = (id: string) => {
		dispatchTodo({ type: "DELETE_TODO", payload: { id } });
	};
	const addTodoHandler = () => {
		if (todoState.listArray.length < 5) {
			const id = Math.random().toString();
			const newTodo = new TodoClass(id, todoState.inputValue, false);

			dispatchTodo({
				type: "ADD_TODO",
				payload: { newTodo },
			});
		}

		clearInputHandler();
	};
	const isEditingHandler = (item: TodoClass) => {
		if (todoState.isEditing) {
			cancelEditingHandler();
		}
		dispatchTodo({
			type: "IS_EDITING_STATUS",
			payload: { status: true, item: item },
		});
		dispatchTodo({
			type: "CHANGE_INPUT_VALUE",
			payload: { enteredValue: item.task },
		});
	};
	const cancelEditingHandler = () => {
		dispatchTodo({
			type: "IS_EDITING_STATUS",
			payload: { status: false, item: {} as TodoClass },
		});
		dispatchTodo({
			type: "CHANGE_INPUT_VALUE",
			payload: { enteredValue: "" },
		});
		dispatchTodo({
			type: "CHECK_INPUT",
			payload: { status: false },
		});
	};

	const confirmEditHandler = () => {
		dispatchTodo({
			type: "CONFIRM_EDIT",
			payload: { itemToEdit: todoState.itemToEdit },
		});
		cancelEditingHandler();
	};

	return {
		listArray: todoState.listArray,
		inputValue: todoState.inputValue,
		isNotEmpty: todoState.isNotEmpty,
		isEditing: todoState.isEditing,
		valueIsValid,
		isDoneItemHandler,
		inputChangeHandler,
		clearInputHandler,
		addTodoHandler,
		deleteTodoHandler,
		isEditingHandler,
		cancelEditingHandler,
		confirmEditHandler,
	};
};

export default useTodo;
