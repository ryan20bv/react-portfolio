import { useReducer } from "react";

interface DiceStateType {
	leftValue: number;
	rightValue: number;
	leftScore: number;
	rightScore: number;
	message: string;
}

const initialState: DiceStateType = {
	leftValue: 6,
	rightValue: 6,
	leftScore: 0,
	rightScore: 0,
	message: "Draw",
};

type DiceActionType =
	| { type: "RESET" }
	| { type: "START" }
	| { type: "RANDOM_LEFT"; payload: { numLeft: number } }
	| { type: "RANDOM_RIGHT"; payload: { numRight: number } }
	| { type: "CHECK" };

const diceReducer = (state: DiceStateType, action: DiceActionType) => {
	switch (action.type) {
		case "RESET":
			return initialState;
		case "START":
			return {
				...state,
				message: "",
			};
		case "RANDOM_LEFT":
			return {
				...state,
				leftValue: action.payload.numLeft,
			};
		case "RANDOM_RIGHT":
			return {
				...state,
				rightValue: action.payload.numRight,
			};
		case "CHECK":
			if (state.leftValue > state.rightValue) {
				state.leftScore++;
				state.message = "1";
			} else if (state.leftValue < state.rightValue) {
				state.rightScore++;
				state.message = "2";
			} else {
				state.message = "Draw";
			}
			return { ...state };
		default:
			return state;
	}
};

const useDice = () => {
	const [diceState, dispatchDice] = useReducer(diceReducer, initialState);

	let intervalId: number;
	const randomNumberHandler = () => {
		const numLeft = Math.ceil(Math.random() * 6);

		const numRight = Math.ceil(Math.random() * 6);
		dispatchDice({ type: "RANDOM_LEFT", payload: { numLeft } });
		dispatchDice({ type: "RANDOM_RIGHT", payload: { numRight } });
	};

	const checkValues = () => {
		dispatchDice({ type: "CHECK" });
	};

	const rollDiceHandler = () => {
		dispatchDice({ type: "START" });
		intervalId = window.setInterval(() => {
			randomNumberHandler();
		}, 200);
		setTimeout(() => {
			clearInterval(intervalId);
			randomNumberHandler();
			checkValues();
		}, 3000);
	};

	const resetHandler = () => {
		dispatchDice({ type: "RESET" });
	};

	const startHandler = () => {
		rollDiceHandler();
	};
	return {
		startHandler: startHandler,
		leftValue: diceState.leftValue,
		rightValue: diceState.rightValue,
		leftScore: diceState.leftScore,
		rightScore: diceState.rightScore,
		message: diceState.message,
		resetHandler: resetHandler,
	};
};

export default useDice;
