import { useState } from "react";

const LionArray = [
	" ",
	" ",
	"Thousand ",
	"Million ",
	"Billion ",
	"Trillion ",
	"Quadrillion ",
	"Quintillion ",
	"Zillion ",
];
// ones
const OnesArray = [
	"Zero ",
	"One ",
	"Two ",
	"Three ",
	"Four ",
	"Five ",
	"Six ",
	"Seven ",
	"Eight ",
	"Nine ",
];
const TeensArray = [
	"Ten ",
	"Eleven ",
	"Twelve ",
	"Thirteen ",
	"Fourteen ",
	"Fifteen ",
	"Sixteen ",
	"Seventeen ",
	"Eighteen ",
	"Nineteen ",
];

const TensArray = [
	"Zero ",
	"Ten ",
	"Twenty ",
	"Thirty ",
	"Forty ",
	"Fifty ",
	"Sixty ",
	"Seventy ",
	"Eighty ",
	"Ninety ",
];

const useNum = (): [string, (input: string) => void] => {
	const [convertedWord, setConvertedWord] = useState<string>("");

	const oneDigitHandler = (data: number): string => {
		return OnesArray[data];
	};
	const teenDigitHandler = (data: number): string => {
		const modulo = data % 10;
		return TeensArray[modulo];
	};
	const twoDigitHandler = (data: number): string => {
		const quotient = Math.floor(data / 10);
		return TensArray[quotient];
	};
	const threeDigitHandler = (data: number): string => {
		const quotient = Math.floor(data / 100);
		return OnesArray[quotient];
	};

	const setConverter = (setData: string): string => {
		let wordOutput = "";
		const originalSet = setData;
		const recursionFN = (input: string) => {
			if (input.length === 3 && +input >= 100) {
				wordOutput += threeDigitHandler(+input);
				wordOutput += " hundred ";
				if (+input % 100 === 0) {
					return;
				}
			} else if (input.length === 2) {
				if (+input < 20 && +input >= 10) {
					wordOutput += teenDigitHandler(+input);
					return;
				} else if (+input >= 20 && +input < 100) {
					wordOutput += twoDigitHandler(+input);
					if (+input % 10 === 0) {
						// wordOutput += twoDigitHandler(+input);
						return;
					}
				}
			} else if (input.length === 1) {
				wordOutput += oneDigitHandler(+input);
				return;
			}
			let newValue = input.slice(1);
			if (!newValue) {
				// showOutput(wordOutput);
				return;
			}
			recursionFN(newValue);
		};
		recursionFN(originalSet);
		return wordOutput;
	};

	const convertEachInArray = (inputArray: string[]) => {
		const inputArrayLength = inputArray.length;
		let wordArray: string[] = [];
		inputArray.forEach((item, index) => {
			let returnWord: string = "";
			if (+item > 0) {
				returnWord = "";
				returnWord += setConverter(item);
				returnWord += LionArray[inputArrayLength - index];
				wordArray.push(returnWord);
			} else if (+item === 0 && inputArrayLength === 1) {
				returnWord += "Zero";
				wordArray.push(returnWord);
			}
		});
		const returnArray = wordArray.join(" , ");
		setConvertedWord(returnArray);
	};
	const outInArrayHandler = (data: string) => {
		let dataCopy = data;
		let dataArray: string[] = [];
		const insertDataFN = (insertData: string) => {
			if (insertData.length > 3) {
				dataArray.unshift(insertData.slice(-3));
				const newData = insertData.slice(0, insertData.length - 3);

				insertDataFN(newData);
			} else {
				dataArray.unshift(insertData);
				// return;
			}
		};
		insertDataFN(dataCopy);

		// setArrayOfNum(dataArray);
		return dataArray;
	};

	const convertHandler = (input: string) => {
		const dataArray: string[] = outInArrayHandler(input);
		convertEachInArray(dataArray);
	};

	return [convertedWord, convertHandler];
};

export default useNum;
