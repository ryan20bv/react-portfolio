import React from "react";

const ModalContent = () => {
	return (
		<div className='border-red-500 h-full'>
			<p>Instruction</p>
			<p>You have to follow the color pattern chosen by the computer</p>
			<p>You are given 5 seconds for each color chosen by the computer</p>
		</div>
	);
};

export default ModalContent;
