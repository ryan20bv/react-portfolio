import React from "react";
import ReactDOM from "react-dom";
import classes from "../../../../../scss/miniApp/simonModal.module.scss";

const Backdrop: React.FC<{ onClose: () => void }> = (props) => {
	return <div className={classes.backdrop} onClick={props.onClose} />;
};

const ModalOverlay: React.FC<{ children: React.ReactNode }> = (props) => {
	return <div className={classes.overlay}>{props.children}</div>;
};

const portalElement = document.getElementById("backdrop-root")! as HTMLElement;

interface PropsType {
	onClose: () => void;
	children: React.ReactNode;
}

const Modal: React.FC<PropsType> = (props) => {
	return (
		<React.Fragment>
			{ReactDOM.createPortal(
				<Backdrop onClose={props.onClose} />,
				portalElement
			)}
			{ReactDOM.createPortal(
				<ModalOverlay>{props.children}</ModalOverlay>,
				portalElement
			)}
		</React.Fragment>
	);
};

export default Modal;
