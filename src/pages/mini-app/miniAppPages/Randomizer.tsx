import React from "react";
import useRandomizer from "./assets/hooks/use-randomizer";

import classes from "../../../scss/miniApp/randomizer.module.scss";
const Randomizer = () => {
	const {
		maxValue,
		arrayValues,
		changeMaxValue,
		singleClickHandler,
		resetStateHandler,
		automaticClickHandler,
	} = useRandomizer();

	const resetButtonHandler = () => {
		resetStateHandler();
	};
	const automaticButtonHandler = () => {
		automaticClickHandler();
	};
	const singleButtonHandler = () => {
		singleClickHandler();
	};
	const changeInputHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		const newMax = e.currentTarget.value;
		changeMaxValue(+newMax);
	};
	return (
		<main className={classes.randomizer}>
			<h1>Randomizer</h1>
			<section className={classes.range}>
				<div>
					<label htmlFor='min'>min: </label>
					<input
						type='number'
						maxLength={3}
						id='min'
						defaultValue='1'
						readOnly
					/>
				</div>
				<div>
					<label htmlFor='max'>max:</label>
					<input
						type='number'
						min={6}
						id='max'
						defaultValue={maxValue}
						onChange={changeInputHandler}
					/>
				</div>
			</section>
			<section className={classes.button}>
				<button className={classes.single} onClick={singleButtonHandler}>
					Single
				</button>
				<button className={classes.reset} onClick={resetButtonHandler}>
					Reset
				</button>
				<button className={classes.automatic} onClick={automaticButtonHandler}>
					Automatic
				</button>
			</section>
			<section className={classes.bottom}>
				{arrayValues.map((item, index) => (
					<div key={index}>
						<p>{+item < 10 ? "0" + item : item}</p>
					</div>
				))}
			</section>
		</main>
	);
};

export default Randomizer;
