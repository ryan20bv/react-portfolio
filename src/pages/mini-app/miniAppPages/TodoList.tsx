import React from "react";

import TodoContent from "./todoListPages/TodoContent";

import useTodo from "./assets/hooks/use-todo";
import { FaTimes, FaPlusCircle, FaCheckCircle } from "react-icons/fa";
import { FcCancel } from "react-icons/fc";
import classes from "../../../scss/miniApp/todoList.module.scss";
import { TodoClass } from "./MiniAppModel";
const SliderOne = () => {
	const {
		listArray,
		isNotEmpty,
		inputValue,
		isEditing,
		valueIsValid,
		isDoneItemHandler,
		inputChangeHandler,
		clearInputHandler,
		addTodoHandler,
		deleteTodoHandler,
		isEditingHandler,
		cancelEditingHandler,
		confirmEditHandler,
	} = useTodo((inputValue: string) => inputValue.trim() !== "");
	const submitHandler = (e: React.FormEvent) => {
		e.preventDefault();

		addTodoHandler();
	};
	const checkboxStatusHandler = (selectedItemId: string) => {
		isDoneItemHandler(selectedItemId);
	};
	const deleteTaskHandler = (itemToDeleteId: string) => {
		deleteTodoHandler(itemToDeleteId);
	};
	const editTaskHandler = (itemToEdit: TodoClass) => {
		isEditingHandler(itemToEdit);
	};
	const cancelEditHandler = () => {
		cancelEditingHandler();
	};
	const confirmHandler = () => {
		confirmEditHandler();
	};
	const changeInputValueHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		inputChangeHandler(e.target.value);
	};
	return (
		<main className={classes.todo}>
			<section className={classes.header}>
				<form action=''>
					<section className={classes.form_left}>
						<input
							type='text'
							placeholder='add todo here'
							maxLength={10}
							onChange={changeInputValueHandler}
							id='input_todo'
							value={inputValue}
							autoComplete='off'
						/>
					</section>
					<section className={classes.form_right}>
						{isNotEmpty && (
							<FaTimes
								className={`${classes.delete_icon} ${classes.icon}`}
								onClick={clearInputHandler}
							/>
						)}

						{valueIsValid && !isEditing && (
							<FaPlusCircle
								className={`${classes.plus_circle} ${classes.icon}`}
								onClick={submitHandler}
							/>
						)}
						{isEditing && (
							<div className={classes.editing_icon}>
								<FcCancel
									className={`${classes.cancel} ${classes.icon}`}
									onClick={cancelEditHandler}
								/>

								{valueIsValid && (
									<FaCheckCircle
										className={`${classes.circle_check} ${classes.icon}`}
										onClick={confirmHandler}
									/>
								)}
							</div>
						)}
					</section>
				</form>
			</section>
			<TodoContent
				listArray={listArray}
				onStrike={checkboxStatusHandler}
				onDelete={deleteTaskHandler}
				onEditing={editTaskHandler}
			/>
		</main>
	);
};

export default SliderOne;
