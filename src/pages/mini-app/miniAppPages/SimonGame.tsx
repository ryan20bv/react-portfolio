import { useRef, useEffect } from "react";
import useSimon from "./assets/hooks/use-simon";
import classes from "../../../scss/miniApp/simon.module.scss";
const SimonGame = () => {
	const greenRef = useRef<HTMLDivElement>(null);
	const redRef = useRef<HTMLDivElement>(null);
	const yellowRef = useRef<HTMLDivElement>(null);
	const blueRef = useRef<HTMLDivElement>(null);
	const wrongRef = useRef<HTMLDivElement>(null);
	const infoRef = useRef<HTMLDivElement>(null);
	const timerRef = useRef<HTMLDivElement>(null);

	const {
		samplePlay,
		timerValue,
		gameLevel,
		gameStatus,
		playStartHandler,
		playerClickHandler,
		isModalOpen,
	} = useSimon(greenRef, redRef, yellowRef, blueRef, wrongRef, timerRef);
	useEffect(() => {
		samplePlay();
	}, [samplePlay]);

	const { isGameOver, playerTurn, computerTurn } = gameStatus;
	let addedClass = playerTurn && classes.computerTurn;
	return (
		<main className={classes.simon}>
			<section className={classes.first}>
				<h1>SIMON</h1>
			</section>
			<section className={classes.playArea}>
				<button className={classes.start} onClick={playStartHandler}>
					{isGameOver && "Game Over"}
					{!isGameOver && playerTurn && "Player Turn"}
					{!isGameOver && computerTurn && "Computer Turn"}
					{!isGameOver && !computerTurn && !playerTurn && "Press To Start"}
				</button>
				<div className={classes.wrapper} ref={wrongRef}>
					<div className={classes.info} ref={infoRef}>
						{isModalOpen && (
							<div>
								<p>Instruction</p>
								<ul>
									<li>
										You have to follow the color pattern chosen by the computer
									</li>
									<li>
										You are given 5 seconds to press each color chosen by the
										computer
									</li>
								</ul>
							</div>
						)}
						{!isModalOpen && (
							<>
								<h1>
									Level <span>{gameLevel}</span>
								</h1>

								<p className={`${classes.timer}`} ref={timerRef}>
									{timerValue}
								</p>
							</>
						)}
					</div>
					<div className={`${classes.playDiv} ${addedClass}`}>
						<div
							className={`${classes.item1}`}
							id='green'
							ref={greenRef}
							onClick={playerClickHandler}
						></div>
						<div
							className={classes.item2}
							id='red'
							ref={redRef}
							onClick={playerClickHandler}
						></div>
						<div
							className={classes.item3}
							id='yellow'
							ref={yellowRef}
							onClick={playerClickHandler}
						></div>
						<div
							className={classes.item4}
							id='blue'
							ref={blueRef}
							onClick={playerClickHandler}
						></div>
					</div>
				</div>
			</section>
			<section className={classes.last}>
				<h1>GAME</h1>
			</section>
		</main>
	);
};

export default SimonGame;
