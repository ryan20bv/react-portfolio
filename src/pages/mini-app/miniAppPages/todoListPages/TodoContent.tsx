import React from "react";
import { FaRegEdit, FaTrashAlt } from "react-icons/fa";
import { TodoClass } from "../MiniAppModel";
import classes from "../../../../scss/miniApp/todoContent.module.scss";

interface PropsType {
	listArray: TodoClass[];
	onStrike: (selectedId: string) => void;
	onDelete: (itemToDeleteId: string) => void;
	onEditing: (itemToEditId: TodoClass) => void;
}

const TodoContent: React.FC<PropsType> = (props) => {
	return (
		<section className={classes.content}>
			{props.listArray.length === 0 && (
				<p className={classes.listEmpty}>Todo List is Empty</p>
			)}
			{props.listArray.length > 0 && (
				<ul className={classes.wrapper}>
					{props.listArray.map((todo) => {
						const classIfDone = todo.isDone ? `${classes.strike}` : "";
						return (
							<li key={todo.id}>
								<section>
									<input
										type='checkbox'
										name=''
										id={todo.id}
										onChange={props.onStrike.bind(null, todo.id)}
										checked={todo.isDone}
									/>
									<p className={classIfDone}>{todo.task}</p>
								</section>
								<div>
									{!todo.isDone && (
										<FaRegEdit
											className={classes.FA_edit}
											onClick={() => props.onEditing(todo)}
										/>
									)}
									<FaTrashAlt
										className={classes.FA_delete}
										// bounce
										onClick={() => props.onDelete(todo.id)}
									/>
								</div>
							</li>
						);
					})}
				</ul>
			)}
		</section>
	);
};

export default TodoContent;
