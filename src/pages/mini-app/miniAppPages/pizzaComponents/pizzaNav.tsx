import React, { useState } from "react";
import { HiShoppingCart, HiSearch, HiXCircle } from "react-icons/hi";

import classes from "../../../../scss/miniApp/pizzaNav.module.scss";

interface PropsType {
	updateSearchValue: (value: string) => void;
}

const PizzaNav: React.FC<PropsType> = ({ updateSearchValue }) => {
	const [inputValue, setInputValue] = useState("");

	const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e.target.value.length > 12) {
			return;
		}
		setInputValue(e.target.value);
		updateSearchValue(e.target.value);
	};
	const clearInputHandler = () => {
		setInputValue("");
		updateSearchValue("");
	};

	return (
		<nav className={classes.pizza_nav}>
			<h1 className={classes.logo}>Pizzalated</h1>
			<div className={classes.input_div}>
				<HiSearch className={`${classes.icon} ${classes.search}`} />

				<input
					type='text'
					placeholder='search here...'
					value={inputValue}
					onChange={changeHandler}
				/>
				{inputValue.trim().length > 0 && (
					<HiXCircle
						className={`${classes.icon} ${classes.clear}`}
						onClick={clearInputHandler}
					/>
				)}
			</div>
			<div className={classes.cart_div}>
				<HiShoppingCart className={`${classes.icon} ${classes.cart}`} />

				<span>3</span>
			</div>
		</nav>
	);
};

export default PizzaNav;
