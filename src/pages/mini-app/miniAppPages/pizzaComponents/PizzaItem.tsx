import React, { useState } from "react";
import { PizzaClass } from "../MiniAppModel";

interface PropsType {
	classes: { [key: string]: string };
	pizza: PizzaClass;
}

const PizzaItem: React.FC<PropsType> = ({ classes, pizza }) => {
	const [qtyValue, setQtyValue] = useState(1);
	const increaseHandler = (): void => {
		setQtyValue((prevValue) => {
			if (prevValue >= 5) {
				return 5;
			}
			return prevValue + 1;
		});
	};
	const decreaseHandler = (): void => {
		setQtyValue((prevValue) => {
			if (prevValue <= 1) {
				return 1;
			}
			return prevValue - 1;
		});
	};

	const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		console.log("You clicked submit");
	};

	return (
		<div className={classes.card}>
			<img src={pizza.img} alt={pizza.flavor} />

			<div className={classes.info}>
				<h1>{pizza.flavor}</h1>
				<p>$ {pizza.price}</p>
			</div>

			<form action='' onSubmit={submitHandler}>
				<div>
					<label htmlFor=''>Qty:</label>
					<button className={classes.btn} onClick={decreaseHandler}>
						-
					</button>
					<input type='text' value={qtyValue} max='5' readOnly />
					<div className={classes.btn} onClick={increaseHandler}>
						+
					</div>
				</div>

				<button type='submit' className={classes.btn_submit}>
					Add to Cart
				</button>
			</form>
		</div>
	);
};

export default PizzaItem;
