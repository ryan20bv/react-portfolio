import React from "react";
import { PizzaClass } from "../MiniAppModel";
import classes from "../../../../scss/miniApp/pizzaContent.module.scss";

import PizzaItem from "./PizzaItem";
const pizzaList = [
	new PizzaClass(
		"101",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_623344781.jpg.webp",
		"Cheesy",
		3.15
	),
	new PizzaClass(
		"102",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_570541132.jpg.webp",
		"Veggie",
		3.16
	),
	new PizzaClass(
		"103",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_184944413.jpg.webp",
		"BBQ Chicken",
		3.15
	),
	new PizzaClass(
		"104",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_313437680.jpg.webp",
		"Hawaiian",
		3.15
	),
	new PizzaClass(
		"105",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_244706695.jpg.webp",
		"Supreme",
		3.15
	),
	new PizzaClass(
		"106",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/Buffalo-Chicken-Pizza-1-1024x683.jpg.webp",
		"Buffalo",
		3.15
	),
	new PizzaClass(
		"107",
		"https://www.ezcater.com/lunchrush/wp-content/uploads/sites/2/2017/10/shutterstock_514457074.jpg.webp",
		"Pepperoni",
		3.15
	),
];

interface PropsType {
	windowWidth: number;
	searchValue: string;
}
const PizzaContent: React.FC<PropsType> = ({ windowWidth, searchValue }) => {
	let foundItem: PizzaClass[] = [];
	pizzaList.forEach((item) => {
		if (item.flavor.toLowerCase().includes(searchValue)) {
			foundItem.push(item);
		}
	});
	if (foundItem.length === 0 || !foundItem) {
		foundItem = pizzaList;
	}
	let pizzaItem1: PizzaClass[] = [];
	let pizzaItem2: PizzaClass[] = [];
	if (windowWidth <= 900) {
		const itemLength = foundItem.length;
		const firstHalfLength = Math.ceil(itemLength / 2);

		foundItem.forEach((item, index) => {
			if (index < firstHalfLength) {
				pizzaItem1.push(item);
			} else {
				pizzaItem2.push(item);
			}
		});
	}

	const isBetween600And900 = windowWidth >= 600 && windowWidth <= 900;
	return (
		<main className={classes.main_content}>
			{!isBetween600And900 && (
				<div className={classes.content}>
					{foundItem.map((pizza) => {
						return <PizzaItem classes={classes} key={pizza.id} pizza={pizza} />;
					})}
				</div>
			)}
			{isBetween600And900 && (
				<>
					<div className={classes.content}>
						{pizzaItem1.map((pizza) => {
							return (
								<PizzaItem classes={classes} key={pizza.id} pizza={pizza} />
							);
						})}
					</div>
					<div className={classes.content}>
						{pizzaItem2.map((pizza) => {
							return (
								<PizzaItem classes={classes} key={pizza.id} pizza={pizza} />
							);
						})}
					</div>
				</>
			)}
		</main>
	);
};

export default PizzaContent;
