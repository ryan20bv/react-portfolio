import React, { useState } from "react";
import useNum from "./assets/hooks/use-num";

import classes from "../../../scss/miniApp/numToWord.module.scss";
const NumToWords = () => {
	const [convertedWord, convertHandler] = useNum();

	const [inputValue, setInputValue] = useState<string>("");
	const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		let enteredValue: string = e.target.value;
		let formatValue = enteredValue.replaceAll(",", "");
		const numRegex: RegExp = /^[0-9\b]+$/;
		const checkedNum = numRegex.test(formatValue);
		if (!checkedNum) {
			if (formatValue.trim().length < 1) {
				setInputValue("");
			}
			return;
		}
		const localeStringValue = parseInt(formatValue).toLocaleString();
		setInputValue(localeStringValue);
	};
	const submitHandler = (e: React.FormEvent) => {
		e.preventDefault();
		let formatValue = inputValue.replaceAll(",", "");
		convertHandler(formatValue);
	};
	let content;
	if (!convertedWord) {
		content = " ";
	} else {
		content = <p>{convertedWord}</p>;
	}
	return (
		<div className={classes.num}>
			<h1>Number to Word</h1>
			<form onSubmit={(e) => submitHandler(e)}>
				<input
					type='text'
					id='numInput'
					value={inputValue}
					maxLength={21}
					onChange={changeHandler}
				/>
				<button>Convert to Word/s</button>
			</form>
			<div>{content}</div>
		</div>
	);
};

export default NumToWords;
