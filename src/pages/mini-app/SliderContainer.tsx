import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import SliderItem from "./SliderItem";
import { AppItemModel } from "./AppItemModel";
import classes from "../../scss/sliderContainer.module.scss";

const miniAppItems = [
	new AppItemModel("todoList.PNG", "todo", "todo"),
	new AppItemModel("simon-game.PNG", "simon game", "simon-game"),
	new AppItemModel("Num-to-word.PNG", "num to word", "num-to-word"),
	new AppItemModel("randomizer.PNG", "randomizer", "randomizer"),
	new AppItemModel("dice-game.PNG", "dice game", "dice-game"),
	new AppItemModel("pizzalated.PNG", "num to word", "pizzalated"),
];

interface PropsType {
	burgerMenuIsOpen: boolean;
}

const SliderContainer: React.FC<PropsType> = (props) => {
	const [contentId, setContentId] = useState("");
	const navigate = useNavigate();
	const clickHandler = (id: string) => {
		if (id === "") {
			return;
		}
		setContentId(id);
	};
	useEffect(() => {
		setContentId("num-to-word");
	}, []);
	useEffect(() => {
		navigate(`/mini-app/${contentId}`);
	}, [contentId, navigate]);
	// const addedClass = props.burgerMenuIsOpen && `${classes.open}`;
	return (
		<div className={`${classes.slider_container} `} id='slider_container'>
			{miniAppItems.map((item) => {
				return (
					<SliderItem
						key={item.id}
						classes={classes}
						item={item}
						clickHandler={clickHandler.bind(null, item.id)}
					/>
				);
			})}
		</div>
	);
};

export default SliderContainer;
