import React from "react";
import { AppItemModel } from "./AppItemModel";

interface PropsType {
	classes: { [key: string]: string };
	item: AppItemModel;
	clickHandler: () => void;
}

const SliderItem: React.FC<PropsType> = ({ classes, item, clickHandler }) => {
	return (
		<div className={classes.slider_content}>
			<img
				src={require(`./miniAppPages/assets/images/${item.src}`)}
				alt={item.alt}
				id={item.id}
				onClick={clickHandler}
			/>
		</div>
	);
};

export default SliderItem;
