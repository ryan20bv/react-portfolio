import React from "react";
import { TechClass } from "./AboutModel";

interface PropsType {
	classes: { [key: string]: string };
	item: TechClass;
}

const AboutIcon: React.FC<PropsType> = ({ classes, item }) => {
	return (
		<div className={classes.card}>
			<img
				// src={process.env.PUBLIC_URL + `/assets/images/${item.img}`}
				src={require(`./images/${item.img}`)}
				alt={item.title}
			/>
			<p>{item.title}</p>
		</div>
	);
};

export default AboutIcon;
