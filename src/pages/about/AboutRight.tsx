import AboutIcon from "./AboutIcon";
import { TechClass } from "./AboutModel";
import classes from "../../scss/aboutRight.module.scss";

const frontEnd = [
	new TechClass("1", "HTML", "html.png"),
	new TechClass("2", "CSS", "css.png"),
	new TechClass("3", "JavaScript", "js.png"),
	new TechClass("4", "React", "react.png"),
];
const backEnd = [new TechClass("5", "NodeJS", "node.png")];
const dataBase = [
	new TechClass("5", "MongoDB", "mongodb.png"),
	new TechClass("6", "Firebase", "firebase.png"),
];
const others = [
	new TechClass("7", "Git", "git.png"),
	new TechClass("8", "PostMan", "postman.png"),
	new TechClass("9", "Redux", "redux.png"),
	new TechClass("10", "Typescript", "typescript.png"),
	new TechClass("11", "Sass", "sass.png"),
	new TechClass("12", "Tailwind", "tailwind.png"),
	new TechClass("13", "Flowbite", "flowbite.png"),
];

const AboutRight = () => {
	return (
		<main className={classes.about_right}>
			<section>
				<h1>About Me</h1>
				<div>
					<h2>
						<b>
							I am from Oil and Gas Industry with Mechanical Engineering
							background specializing in QA/QC to Information and Technology in
							Web Developer using MERN Stack.
						</b>
					</h2>
					<p>
						As QA/QC in Oil and Gas Industry, the task is to ensure that step by
						step procedure must be followed to meet the quality and standard of
						the project.
					</p>
					<p>
						The skills are keen to details, ensure different department function
						well and innovate task and activities and pass information
						efficiently.
					</p>{" "}
					<p>
						As for Web Developer, i was curious and want to learn new skill. I
						enrolled in a boot camp and then do self learning to determine if i
						have a grit for Information and Technology. <br />
						Now i have several project deployed using MERN stack, and looking
						for working environment to use my new acquired skill and further
						grow in the field of Information and Technology.
					</p>
				</div>
			</section>
			<section>
				<h1>Tech Stack</h1>
				<div className={classes.section}>
					<p>
						<b>Front-End</b>
					</p>
					<div className={classes.picture}>
						{frontEnd.map((item) => {
							return <AboutIcon key={item.id} item={item} classes={classes} />;
						})}
					</div>
				</div>
				<div className={classes.section}>
					<p>
						<b>Back-End</b>
					</p>
					<div className={classes.picture}>
						{backEnd.map((item) => {
							return <AboutIcon key={item.id} item={item} classes={classes} />;
						})}
					</div>
				</div>
				<div className={classes.section}>
					<p>
						<b>Data Base</b>
					</p>
					<div className={classes.picture}>
						{dataBase.map((item) => {
							return <AboutIcon key={item.id} item={item} classes={classes} />;
						})}
					</div>
				</div>
				<div className={classes.section}>
					<p>
						<b>Others</b>
					</p>
					<div className={classes.picture}>
						{others.map((item) => {
							return <AboutIcon key={item.id} item={item} classes={classes} />;
						})}
					</div>
				</div>
			</section>
		</main>
	);
};

export default AboutRight;
