import React from "react";

import Nav from "../../ui/Nav";
import Footer from "../../ui/Footer";
import classes from "../../scss/home.module.scss";

const Home = () => {
	return (
		<div className={classes.home}>
			<Nav
				head='Ryan Valenzuela'
				fontFamily='Great Vibes'
				headColor='white'
				listColor='red'
				addedClass={classes.home_responsive}
			/>
			<Footer />
		</div>
	);
};

export default Home;
